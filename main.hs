import System.IO
import Control.Monad
import System.Random
import Data.Char
import Data.List
import Data.Function
import System.CPUTime
import System.IO.Error
import Control.Exception

--Getting the words from the text file

getLines::FilePath -> IO [String]
getLines = liftM lines . readFile


--Calling main starts the program

main = do
    putStrLn "Hangman Game by Oshi"
    list <- getLines "wordlist.txt"

    putStr "Please enter your name: \n>"
    name <- getLine
    startTime <- getCPUTime
    showDiffPrompt list
    endTime <- getCPUTime
    let score = (name,quot (endTime - startTime) 2500000)
    addToFileList "highscore.hangman" score
    putStrLn "Play Again (Yes/No)?"
    putStr ">"
    again <- getLine
    if(map toLower again == "yes") then
        do main
    else
        putStrLn "Bye..."
    putStrLn "highscores : -"
    getScores "highscore.hangman"
        



--shows the difficulty option to the user then calls the getword and getword methods


showDiffPrompt::[String]->IO()
showDiffPrompt xs = do
    putStr "Pick a Difficulty Level(1,2,3):\n1)Easy(Under 4 Letters)\n2)Medium(Under 6 Letters)\n3)Hard(Over 6 Letters)\n>"
    diff <- getLine
    let diffNum = (read diff :: Int)
    let gameWordList = getWords diffNum xs --gettings words according to the difficulty level
    let listLength = length gameWordList  --getting the length of all the words returned to generate a random number
    gen <- newStdGen
    let (wordIndex,newGen) =  randomR (1,listLength)gen :: (Int, StdGen) 
    let word = gameWordList!!wordIndex --getting the word from the list
    --putStrLn $ word
    getGuess word []



--gets the word from the list according to the difficulty level

getWords::Int->[String]->[String]
getWords n xs
            |n == 1 = filter(\x->length x < 4) xs
            |n == 2 = filter(\x->length x < 6 && length x >= 4) xs
            |n == 3 = filter(\x->length x >= 6) xs
            |otherwise = error ("Invalid Selection")




--Checks if the guessed word is an elem of the original word and returns it in dashed(---) form

match :: String->String->String
match word guess = [if elem x guess then x else '-' | x<-word]



--returns a boolean value depending if all the guessed letters so far are in the original word

isElem::String->String->Bool
isElem a b = and [if elem x b then True else False|x<-a]



--takes the input from the user and then calls check guess method

getGuess::String->String->IO()
getGuess word guess = do 
    putStrLn $ match word guess
    putStr "Enter Your guess > "
    userGuess <- getLine
    checkGuess word userGuess guess




{--checks if the guess from the previous function is a letter in the original word , also calls the showlives function and the 
get guessfunction untill the user runs out of life or guesses the word correctly--}

checkGuess::String->String->String->IO()
checkGuess word guess oldGuess= 
    if(length (newGuess\\word) <= 6)
     then
        if(map toLower word == map toLower guess || isElem word newGuess) 
            then do putStrLn $ "You Win! The word was "++word
            else do 
                putStrLn ""
                showLives (7-(length (newGuess\\word))) ""
                --putStrLn $ show (7-(length (newGuess\\word)))
                getGuess word $ newGuess
        else do
             putStrLn ""
             showLives 0 word
    where newGuess = match (guess++oldGuess) word



--displays the users lives as stickmen

showLives::(Num a, Eq a)=>a->String->IO()
showLives 0 word = putStrLn $ "  _____\n  |/  |    ***YOU ARE DEAD***\n  |   O    THE WORD WAS " ++word++ "\n  |  /|\\\n  |   /\\\n  |______"
showLives 1 _ = putStrLn "  _____\n  |/  |\n  |   O\n  |  /|\\\n  |   /\n  |______                        "
showLives 2 _ = putStrLn "  _____\n  |/  |\n  |   O\n  |  /|\\\n  |\n  |______                            "
showLives 3 _ = putStrLn "  _____\n  |/  |\n  |   O\n  |   |\\\n  |\n  |______                            "
showLives 4 _ = putStrLn "  _____\n  |/  |\n  |   O\n  |   |\n  |\n  |______                              "
showLives 5 _ = putStrLn "  _____\n  |/  |\n  |   O\n  |\n  |\n  |______                                  "
showLives 6 _ = putStrLn "  _____\n  |/  |\n  |\n  |\n  |\n  |______                                      "
--showLives _ _ = putStrLn "  _____\n  |/  |\n  |\n  |\n  |\n  |______                                      "
showLives _ _ = putStrLn ""


--reads the score from the list and parses it into a list of tuple [(String,int)], then sorts it by time taken and prints it

getScores::FilePath->IO()
--getScores :: FilePath-> IO [(String, Int)]
getScores f = do
    contents <- readFile f
    let scores = read contents :: [(String,Int)]
    let sortedList = take 3 $  sortBy (compare `on` snd) scores
    mapM_ putStrLn $ map printTuple sortedList




--to append to a list, i read the file, append the element to the list then write the file  

addToFileList :: (Read a, Show a) => FilePath -> a -> IO ()
addToFileList fp a = do olds <- readFile fp `catch` \e ->
                                if isDoesNotExistError e 
                                then return "[]"
                                else ioError e
                        let oldl = read olds
                            newl = oldl ++ [a]
                            news = show newl
                        length news `seq`
                               writeFile fp news

--Source -
{--
http://stackoverflow.com/questions/8110567/getting-error-prelude-read-no-parse
--}



--Prints a tuple to the console

printTuple :: (Show a, Show b) => (a, b) -> String
printTuple (a, b) = show a ++ " - " ++ show b